package serabass

import groovy.json.JsonSlurper
import org.omg.CORBA.portable.Delegate

// TODO Поменять на self.text.json итд
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.nio.file.Path
import java.util.zip.ZipInputStream

class ExtendCategory {

    private static inflectCacheData = [:]

    /** <URL> */
    // Получаем json по ссылке и парсим его налету
    static getJson(URL self) {
        URLConnection connection = self.openConnection()

        String contentType = connection.contentType

        if (!(contentType.startsWith('application/json'))
            && !contentType.startsWith('application/x-javascript'))
            throw new Exception("Wrong Content-Type: ${connection.contentType}")

        new JsonSlurper().parseText(self.text)
    }

    // Получаем xml по ссылке и парсим его налету
    static Node getXml(URL self) {
        URLConnection connection = self.openConnection()

        if (!(connection.contentType.startsWith('text/xml')))
            throw new Exception("Wrong Content-Type: ${connection.contentType}")

        new XmlParser().parseText(self.text)
    }

    // Получаем html по ссылке и парсим его налету
    static Node getHtml(URL self) { self.xml }

    static BufferedImage getImage(URL self) {
        URLConnection connection = self.openConnection()

        if (!(connection.contentType.startsWith('image/')))
            throw new Exception("Wrong Content-Type: ${connection.contentType}")

        ImageIO.read(new ByteArrayInputStream(self.bytes))
    }

    static ZipInputStream getZip(URL self) {
        URLConnection connection = self.openConnection()

        if (!(connection.contentType.startsWith('application/zip')))
            throw new Exception("Wrong Content-Type: ${connection.contentType}")

        new ZipInputStream(new ByteArrayInputStream(self.bytes))
    }

    static Object fetch(URL self) {
        URLConnection connection = self.openConnection()

        switch(true) {
            case connection.contentType.startsWith('text/xml') :
            case connection.contentType.startsWith('text/html') :
                return new XmlParser().parseText(self.text)

            case connection.contentType.startsWith('application/json') :
                return new JsonSlurper().parseText(self.text)

            case connection.contentType.startsWith('image/') :
                return ImageIO.read(new ByteArrayInputStream(self.bytes))

            case connection.contentType.startsWith('application/zip') :
                return new ZipInputStream(new ByteArrayInputStream(self.bytes))

        }
        self.text
    }

    static String getContentType(URL self) {
        self.openConnection().contentType
    }

    static URL save(URL self, FileOutputStream stream) {
        stream.write(self.bytes)
        self
    }

    static URL save(URL self, String path) {
        new FileOutputStream(path).write(self.bytes)
        self
    }

    static URL save(URL self) {
        new FileOutputStream(self.path.split('/').last()).write(self.bytes)
        self
    }
    /** </URL> */

    /** <Byte> */

    static Number getB (Number self) { self               } // Байт (необязательно)
    static Number getKb(Number self) { self * (1024 ** 1) } // Килобайт
    static Number getMb(Number self) { self * (1024 ** 2) } // Мегабайт
    static Number getGb(Number self) { self * (1024 ** 3) } // Гигабайт
    static Number getTb(Number self) { self * (1024 ** 4) } // Терабайт
    static Number getPb(Number self) { self * (1024 ** 5) } // Петабайт
    static Number getEb(Number self) { self * (1024 ** 6) } // Эксабайт

    // TODO Заработало, но доработать.
    static String size(Number self, String unit = 'b') {
        def index = [ 'b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb'].findIndexOf { val -> val.toLowerCase() == unit.toLowerCase() }

        if (index < 0)
            throw new Exception('Неверно указана единица')

        (self ** ++index) + unit
    }

    /** </Byte> */

    /** <String> */

    /**
     * Выполняем groovy-код из строки
     * @param self
     * @return
     */
    static Object eval(String self, Map<String, Object> variables) {
        def shell = new GroovyShell()

        variables.each { String k, Object v ->
            shell.setVariable(k, v)
        }

        shell.evaluate(self)
    }

    /**
     * Выполняем groovy-код из строки
     * @param self
     * @return
     */
    static Object eval(String self) {
        use ExtendCategory, { self.eval([:]) }
    }

    /**
     * Склоняем слово по падежам с помощью Яндекса
     * @param self
     * @return
     */
    static Object inflect(String self) {
        if (inflectCacheData[self] == null)
            inflectCacheData[self] = "http://export.yandex.ru/inflect.xml?name=${self}&format=json".url.json

        inflectCacheData[self]
    }

    static URL getUrl(String self) {
        self.toURL()
    }

    /** </String> */

    /** <Random> */
    static int random(Number self, boolean asInteger = false) {
        def result = self * Math.random()
        asInteger ? Math.round(result) : result
    }

    static int getRandom(Number self) { use ExtendCategory, { self.random(true) } }

    static String random(String self, boolean asRange = true) {
        use Random, {
            if (asRange) {
                ArrayList<Object> values = [(self.size() - 1).random, (self.size() - 1).random]
                self.substring((int) values.min(), (int) values.max())
            } else
                self[(self.size() - 1).random]
        }
    }

    static char getRandom(String self) { use ExtendCategory, { self.random() } }

    static List random(List self) {
        use ExtendCategory, {
            def values = [(self.size() - 1).random, (self.size() - 1).random]
            self[values.min()..values.max()]
        }
    }

    static List random(Object[] self) {
        use ExtendCategory, {
            (self as List).random
        }
    }

    static Object getRandom(List self) {
        use ExtendCategory, { self[(self.size() - 1).random] }
    }

    static Object getRandom(Range self) {
        use ExtendCategory, { self[(self.size() - 1).random] }
    }

    static String getRandomLine(File file) {
        use ExtendCategory, { file.readLines().random }
    }

    static String getRandomLine(File file, String charset) {
        use ExtendCategory, { file.readLines(charset).random }
    }

    static String getRandomLine(URL url, String charset) {
        use ExtendCategory, { url.readLines(charset).random }
    }

    static String getRandomByte(File file) {
        use ExtendCategory, { file.readBytes().toList().random }
    }

    /** </Random> */

    /** <Currency> */
    // Функции-алиасы для определённых валют (добавляем свои, если нужно)
    public static Number getUsd(Number self) { Currency.get('USD', self) }
    public static Number getRub(Number self) { Currency.get('RUB', self) }
    public static Number getEur(Number self) { Currency.get('EUR', self) }
    public static Number getKzt(Number self) { Currency.get('KZT', self) }
    /** </Currency> */

    /** <Closure> */
    public static Closure plus(Closure self, Closure closure) {
        return {
            def result =  [self, closure]
            result*.delegate = delegate
            result*.call()
        }
    }

    public static Closure next(Closure self) {
        return {[self, self]*.call()}
    }

    public static Closure multiply(Closure self, Number count) {
        return {
            def result = []
            count.times {
                result.add(self())
            }
            result
        }
    }

    public static Closure leftShift(Closure self, Closure closure) { self + closure }

    /** </Closure> */
}
