package serabass

class Currency {
    private static _url = 'http://rate-exchange.appspot.com/currency?from={from}&to={to}'

    /**
     * Основная валюта, от которой будем высчитывать остальные
     */
    public static mainCurrency = 'KZT'

    private static currencyCachedData = [:]

    /**
     * Подгружаем информацию о соотношениях валют
     * @param from
     * @param to
     * @return
     */
    private static loadExchange(String from, String to) {
        // TODO Замутить кеширование JSON'а в файле (или Memcache)
        def result = use ExtendCategory, {
            _url.replace('{from}', from).replace('{to}', to).url.json
        }

        if (result.err == null) {
            return Math.round( (float) result.rate * 100) / 100
        }
    }

    /**
     * Получаем сумму в новой валюте
     * @param currency
     * @param value
     * @return
     */
    public static Number get(String currency, Number value) {
        if (currencyCachedData[currency] == null) {
            currencyCachedData[currency] = loadExchange(currency, mainCurrency)
        }

        currencyCachedData[currency] * value
    }
}
