package serabass

class Random {
    public static class Generate {

        // TODO Разобраться, почему не вызывается
        public static ArrayList list(Integer size, Closure callback) {
            def result = []
            size.times { result.add(callback()) }
            result
        }

        public static ArrayList list(Integer size = null) {
            use Random, { list(size?:100, { 100.random }) }
        }

        public static String string(Integer size = 8, Character[] chars = ('a'..'z')+('A'..'Z')+('0'..'9')) {
            def result = ""
            def aChars = new  ArrayList<Character>()

            chars.each { c -> aChars.add(c) }

            size.times { use Random, { result += aChars.random } }

            result
        }

        public static File file(String path, Integer size = 1024, Character[] chars = (0..255)) {
            def file = new File(path)
            file.write(string(size, chars))
            file
        }
    }
}
