
class Node<T> {
    List<Node<T>> children = []

    public sum(String prop) {
        if (children.size() == 0)
            return this[prop]

        children.sum {
            it.sum(prop)
        }
    }
}

class NodeBuilder<T> extends BuilderSupport {
    def children = []

    protected void setParent(parent, child){
        parent.children << child
    }

    protected Object createNode(Object name) {
        name == 'root' ? this : new T()
    }

    protected Object createNode(Object name, Object value) {
        null
    }

    protected Object createNode(Object name, Map attributes) {
        name == 'node' ? new T( attributes ) : null
    }

    protected Object createNode(Object name, Map attributes, Object value) {
        null
    }

    protected void nodeCompleted(Object parent, Object node) {

    }

    public Number sum(String prop) {
        def result = 0
        children.each {
            result += it.sum(prop);
        }
        result
    }

    Iterator iterator() { children.iterator() }
}

class Task extends Node {
    public Number time = 0
    public Number price = 0
}

def b = new NodeBuilder<Task>().root() {
    node time: 1, price: 100
    node {
        node time: 2, price: 100
        node time: 7, price: 100
        node time: 1, price: 100
    }
    node {
        node time: 1, price: 100
        node {
            node time: 10, price: 1000
        }
    }
}

println b.sum('price')
