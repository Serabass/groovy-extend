import groovy.time.TimeCategory
import serabass.ExtendCategory

use ExtendCategory, {
    Closure c1 = { println 1 };
    Closure c2 = { println 2 };
    Closure c3 = c1 + c2
    c3.call()
}